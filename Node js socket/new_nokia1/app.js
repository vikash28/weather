//const translate = require('google-translate-api');
//console.log('translation json: '+JSON.stringify(translate)  );

/*translate('Ik spreek Engels', {to: 'es'}).then(res => {
    console.log(res.text);
    //=> I speak English
    console.log(res.from.language.iso);
    //=> nl
}).catch(err => {
    console.error(err);
});
*/
/*translate('I spea Dutch!', {from: 'en', to: 'nl'}).then(res => {
    console.log(res.text);
    //=> Ik spreek Nederlands!
    console.log(res.from.text.autoCorrected);
    //=> true
    console.log(res.from.text.value);
    //=> I [speak] Dutch!
    console.log(res.from.text.didYouMean);
    //=> false
}).catch(err => {
    console.error(err);
});*/

/**
 * Copyright 2016 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

require ( 'dotenv' ).config ( {silent: true} );
var express = require ( 'express' );
var compression = require ( 'compression' );
var bodyParser = require ( 'body-parser' );  // parser for post requests
var watson = require ( 'watson-developer-cloud' );
const translate = require('google-translate-api');
var path    = require("path"); 

//The app owner may optionally configure a cloudand db to track user input.
//This cloudand db is not required, the app will operate without it.
//If logging is enabled the app must also enable basic auth to secure logging
//endpoints
 

//The conversation workspace id
var workspace_id = process.env.WORKSPACE_ID || '1a15a6b8-4231-4ddb-91f8-9df513cb2f8b';
var logs = null;

var app = express ();

app.use ( compression () );
app.use ( bodyParser.json () );
//static folder containing UI
app.use ( express.static ( __dirname + "/dist" ) );

// Create the service wrapper
var conversation = watson.conversation ( {
  username: process.env.CONVERSATION_USERNAME || '8c072b90-f189-4992-9217-87fd8e3b250f',
  password: process.env.CONVERSATION_PASSWORD || 'aWU7cnrAEG7o',
  version_date: '2016-07-11',
  version: 'v1'
} );

/*//create the translator
var language_translation = watson.language_translator({
  username: '3d0a8149-4b2c-471b-9979-0066149bf3b4',
  password: 'cZ8kCDVv1T5U',
  version: 'v2'
});*/
 
app.post ( '/api/translation', function (req, res) {

   //console.log('translation: '+  translate);

   translate( req.body.text , {to: req.body.language}).then(sucess => {
    //console.log(sucess.text);
    //console.log(sucess.from.language.iso);
    //=> nl
     return res.json ( {
      'output':   sucess
    } );
	}).catch(err => {
    //console.error(err);
     return res.json ( {
      'output':   err
    } );
	});

   /*language_translation.translate({
    text: req.body.text,
    source: req.body.source,
    target: req.body.target
  }, function(err, translation) {
    if (err)
    {
    console.log("error"+JSON.stringify(err));
     return res.json ({
      'output':   err
    } );
    }
    else
    {  console.log("sucess"+JSON.stringify(translation));
    return res.json ( {
      'output':   translation
    } );
    }
});
 */

});
 


// Endpoint to be call from the client side
app.post ( '/api/message', function (req, res) {
 console.log('test: '+req);

  if ( !workspace_id || workspace_id !== '1a15a6b8-4231-4ddb-91f8-9df513cb2f8b' ) {
    //If the workspace id is not specified notify the user
    return res.json ( {
      'output': {
        'text': 'The apps has not been configured with a <b>WORKSPACE_ID</b> environment variable. Please refer to the ' +
        '<a href="https://github.com/watson-developer-cloud/car-dashboard">README</a> documentation on how to set this variable. <br>' +
        'Once a workspace has been defined the intents may be imported from ' +
        '<a href="https://github.com/watson-developer-cloud/car-dashboard/blob/master/training/car_workspace.json">here</a> in order to get a working application.'
      }
    } );
  }
  var payload = {
    workspace_id: workspace_id,
    context: {}
  };
  if ( req.body ) {
    if ( req.body.input ) {
      payload.input = req.body.input;
    }
    if ( req.body.context ) {
      // The client must maintain context/state
      payload.context = req.body.context;
    }
  }
  // Send the input to the conversation service
  conversation.message ( payload, function (err, data) {
    if ( err ) {
      console.error ( JSON.stringify ( err ) );
      return res.status ( err.code || 500 ).json ( err );
    }
    if ( logs ) {
      //If the logs db is set, then we want to record all input and responses
      var id = uuid.v4 ();
      logs.insert ( {'_id': id, 'request': payload, 'response': data, 'time': new Date ()}, function (err, data) {

      } );
    }
    return res.json ( data );
  } );
} );

module.exports = app;
