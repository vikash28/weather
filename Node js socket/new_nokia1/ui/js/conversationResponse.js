/*
 * Copyright © 2016 I.B.M. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the “License”);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* The Intents module contains a list of the possible intents that might be returned by the API */

/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "^ConversationResponse$" }] */
/* global Animations: true, Api: true, Panel: true */

var ConversationResponse = (function() {
  'use strict';
  var responseFunctions;

  return {
    init: init,
    responseHandler: responseHandler,
    smevalueonlanguage: smevalueonlanguage,
    pageonlanguage:pageonlanguage

  };

  function init() {
    setupResponseHandling();
  }

  function smevalueonlanguage(valueobj)
  {
    updatesmevalueonlanguage(valueobj);
  }
   
  function pageonlanguage(valueobj)
  {
    updatepageonlanguage(valueobj);
  }
   
  // Create a callback when a new Watson response is received to handle Watson's response
  function setupResponseHandling() {
    var currentResponsePayloadSetter = Api.setWatsonPayload;
    Api.setWatsonPayload = function(payload) {
      currentResponsePayloadSetter.call(Api, payload);
      responseHandler(payload);
    };
  }

   
   

  // Called when a Watson response is received, manages the behavior of the app based
  // on the user intent that was determined by Watson
  function responseHandler(data) {
   if (data && data.intents && data.entities) // && !data.output.error
      {
      var primaryIntent = data.context.call_api;  
      if (primaryIntent =='true' && data.context.call_api !== undefined ) {
               handleBasicCaseSME( data);
      }
    }
  }

  // Handles the case where there is valid intent and entities
   function  handleBasicCaseSME( data)
   {
       var resultJson = {
      'tagName': 'li',
      'attributes' : [{'name':'id','value':'resultlistitem'}],
      'classNames': ['hide'] ,
      'children': [{
         'tagName': 'div',
        'classNames': ['media-body'],
         'children': [{
                      'tagName': 'div',
                       'classNames': ['media' ] , 
                        'children':[{
                            'tagName': 'div',
                            'classNames': ['media-body'] ,
                            'html' : ' <h5><b>Please Wait</b></h5><a href="javascript:void(0);" style="color: #000000"> <i class="fa fa-spinner faa-spin animated fa-5x"></i></a>'
                      }]
               }]
          }] 
      };

               
        var resultDiv=Common.buildDomElement(resultJson);
        var resultElement = document.getElementById('resultlist');
        resultElement.innerHTML='';
        resultElement.appendChild(resultDiv);
        Common.show(document.getElementById('resultlistitem'));

        var messageEndpoint="http://localhost:8080/IDBI/TestServlet?project=Water&country=Argentina";
        var http = new XMLHttpRequest();       
        http.open('GET', messageEndpoint, true);
        //http.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        http.onload = function() {
        if (http.status === 200  && http.responseText) {
        
           var resultJson1 = {
          'tagName': 'li',
          'attributes' : [{'name':'id','value':'resultlistitem'}],
          'classNames': ['hide'] ,
          'children': [{
          'tagName': 'div',
          'classNames': ['media-body'],
          'children': [{
                      'tagName': 'div',
                       'classNames': ['media' ] , 
                        'children':[{
                            'tagName': 'div',
                            'classNames': ['media-body'] ,
                            'html' : '<h5><b>Done....</b></h5><a href="javascript:void(0);" style="color: #00b4a0"> <i class="fa fa-thumbs-o-up faa-bounce animated fa-5x"></i></a>'
                      }]
               }]
          }] 
      };

               
          var resultDiv1=Common.buildDomElement(resultJson1);
          var resultElement = document.getElementById('resultlist');
          resultElement.innerHTML='';
          resultElement.appendChild(resultDiv1);
          Common.show(document.getElementById('resultlistitem'));
 

 
           var response=JSON.parse(http.responseText )
           if(response !="")
           {
           setTimeout(function(){
             Common.hide(document.getElementById('resultlistitem'));
             document.getElementById('resultlist').innerHTML='';
             if(language =='en'){
                $.each(response, function (index, valueobj) {
                updatesmevalueonlanguage(valueobj);
                });
                }
                else
                {
                $.each(response, function (index, valueobj) {
                 // alert(JSON.stringify(valueobj));
                Api.gethttpMessageTranslation(valueobj,'sme');
                });
                }
            }, 2500);
           }  
         }
         };
         http.onerror = function() {
         alert('Network error trying to send message!');
         };
         http.send(); 
   }

  // Calls the appropriate response function based on the given intent and entity returned by Watson
  function  updatesmevalueonlanguage(valueobj){
          //alert(valueobj);
          var resultJson = {
          'tagName': 'li',
           'classNames': ['media'] ,
          'children': [{
          'tagName': 'div',
          'classNames': ['media-body'],
          'children': [{
                      'tagName': 'div',
                       'classNames': ['media' ] ,
                       'html':'<a class="pull-left" target="_blank" href="'+( (language =='en') ? valueobj.sme_profile : valueobj[3])+'"><img class="media-object img-circle" style="max-height:40px;" src="images/dummy.jpg" /></a>', 
                        'children':[{
                            'tagName': 'div',
                            'classNames': ['media-body'] ,
                            'html' : ( (language =='en') ? '<h5>'+valueobj.name+' | '+valueobj.country+'</h5>  <small class="text-muted"> Weightage-'+ valueobj.weightage+' & Skills- '+valueobj.skills+'</small> ' : '<h5>'+valueobj[4]+' | '+valueobj[5]+'</h5>  <small class="text-muted">'+valueobj[1]+'-'+ valueobj[6]+' & '+valueobj[2]+'- '+valueobj[0]+'</small>' )
                      }]
               }]
          }] 
          };
 
               
          var resultDiv=Common.buildDomElement(resultJson);
          var resultElement = document.getElementById('resultlist');
          resultElement.appendChild(resultDiv); 
           
  }

    // Calls the appropriate response function based on the given intent and entity returned by Watson
  function  updatepageonlanguage(valueobj){
              document.getElementById('recentrequest').innerHTML=valueobj[0];
              document.getElementById('result').innerHTML=valueobj[1];
              waitingDialog.hide();        
  }


}());
